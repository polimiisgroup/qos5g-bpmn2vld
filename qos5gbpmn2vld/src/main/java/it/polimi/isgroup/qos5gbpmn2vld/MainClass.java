package it.polimi.isgroup.qos5gbpmn2vld;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

public class MainClass {

	public static void main(String[] args) {
		Translator t = new Translator();
		try {
			if (args.length < 3) {
				t.translate(null, System.getProperty("user.dir"));
			} else if (args.length < 4){
				t.translate(args[2], System.getProperty("user.dir"));
			} else {
				t.translate(args[2], args[3]);
			}
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
