package it.polimi.isgroup.qos5gbpmn2vld;

import it.polimi.isgroup.qos5gbpmn2vld.dataStructures.QoSMetric;
import it.polimi.isgroup.qos5gbpmn2vld.dataStructures.VLDConfig;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Translator {

	public void translate(String inputFile, String outputFolder)
			throws ParserConfigurationException, SAXException, IOException {
		File file = null;
		if (inputFile == null){
			file = new File(getClass().getClassLoader()
					.getResource("hazardousGoodsShipment.bpmn").getFile());
		} else {
			file = new File(inputFile);
		}
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		System.out.println("it parsed");
		NodeList nList = doc.getElementsByTagName("bpmn:task");
		for (int i = 0; i < nList.getLength(); i++) {
			Node taskNode = nList.item(i);
			if (taskNode.getNodeType() == Node.ELEMENT_NODE) {
				Element taskElement = (Element) taskNode;
				String activityId = taskElement.getAttribute("id");
				if (taskElement.getElementsByTagName("bpmn:extensionElements")
						.getLength() > 0) {
					if (taskElement
							.getElementsByTagName("bpmn:extensionElements")
							.item(0).getNodeType() == Node.ELEMENT_NODE) {
						Element startTime = null;
						Element endTime = null;
						Element location = null;
						// Time information
						NodeList extensions = ((Element) taskElement
								.getElementsByTagName("bpmn:extensionElements")
								.item(0))
								.getElementsByTagName("qos5g:startTime");
						if (extensions.getLength() > 0) {
							startTime = (Element) extensions.item(0);
						}
						extensions = ((Element) taskElement
								.getElementsByTagName("bpmn:extensionElements")
								.item(0)).getElementsByTagName("qos5g:endTime");
						if (extensions.getLength() > 0) {
							endTime = (Element) extensions.item(0);
						}
						// Location information
						extensions = ((Element) taskElement
								.getElementsByTagName("bpmn:extensionElements")
								.item(0))
								.getElementsByTagName("qos5g:location");
						if (extensions.getLength() > 0) {
							location = (Element) extensions.item(0);
						}
						// Lifecycle information
						extensions = ((Element) taskElement
								.getElementsByTagName("bpmn:extensionElements")
								.item(0)).getElementsByTagName("qos5g:states");
						if (extensions.getLength() > 0) {
							NodeList states = ((Element) extensions.item(0))
									.getElementsByTagName("qos5g:state");
							for (int j = 0; j < states.getLength(); j++) {
								Node stateNode = states.item(j);
								if (stateNode.getNodeType() == Node.ELEMENT_NODE) {
									Element stateElement = (Element) stateNode;
									String stateId = stateElement
											.getAttribute("id");
									// Find artifacts for current state
									if (stateElement.getElementsByTagName(
											"qos5g:artifacts").getLength() > 0) {
										NodeList artifacts = ((Element) stateElement
												.getElementsByTagName(
														"qos5g:artifacts")
												.item(0))
												.getElementsByTagName("qos5g:artifact");
										for (int k = 0; k < artifacts
												.getLength(); k++) {
											Node artifactNode = artifacts
													.item(k);
											if (artifactNode.getNodeType() == Node.ELEMENT_NODE) {
												Element artifactElement = (Element) artifactNode;
												String artifactId = artifactElement
														.getAttribute("id");
												System.out.println("Activity "
														+ activityId
														+ ", State " + stateId
														+ ", Artifact "
														+ artifactId);

												// Create output VLD data
												VLDConfig vld = new VLDConfig();
												vld.setId(artifactId + "_"
														+ activityId + "_"
														+ stateId);
												vld.setDescription(getTime(
														startTime, endTime)
														+ getLocation(location, stateId));
												vld.setQos(getQos(artifactElement));
												writeFile(vld, outputFolder);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	private void writeFile(VLDConfig vld, String outputFolder) {
		JSONObject obj = new JSONObject();
		obj.put("id", vld.getId());
		obj.put("vendor", vld.getVendor());
		obj.put("descriptor_version", vld.getDescriptor_version());
		obj.put("description", vld.getDescription());
		obj.put("root_requirement", vld.getRoot_requirement());
		obj.put("test_access", vld.getTest_access());
		JSONArray qos = new JSONArray();
		for (QoSMetric metric: vld.getQos()) {
			JSONObject qosMetric = new JSONObject();
			qosMetric.put(metric.getMetric(), metric.getValue());
			qos.add(qosMetric);
		}
		obj.put("qos", qos);
		obj.put("connectivity_type", vld.getConnectivity_type());
		
		System.out.println(obj.toJSONString());
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0) {
			outputFolder = outputFolder + "\\";
		} else {
			outputFolder = outputFolder + "/";
		}
		try (FileWriter file = new FileWriter(outputFolder + "" + vld.getId() + ".json")) {
            file.write(obj.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }
	}

	private ArrayList<QoSMetric> getQos(Element artifactElement) {
		NodeList metrics = (artifactElement.getElementsByTagName("qos5g:metric"));
		ArrayList<QoSMetric> result = new ArrayList<QoSMetric>();
		for (int k = 0; k < metrics
				.getLength(); k++) {
			Node metricNode = metrics
					.item(k);
			if (metricNode.getNodeType() == Node.ELEMENT_NODE) {
				Element metricElement = (Element) metricNode;
				String metricId = metricElement
						.getAttribute("id");
				if (metricId != null) {
					QoSMetric metric = new QoSMetric(); 
					metric.setMetric(metricId);
					metric.setValue(metricElement
						.getAttribute("value"));
					result.add(metric);
				}
			}
		}
		return result;
	}

	private String getLocation(Element location, String stateId) {
		String result = "";
		String type = location.getAttribute("type");
		if (stateId.equals("Init") || stateId.equals("Ready")){
			if(type.equals("Area")){
				result = "Location='" + location.getAttribute("area") + "'";
			} else {
				result = "Location='" + location.getAttribute("start") + "'";
			}
		} else if (stateId.equals("Running")){
			if(type.equals("Area")){
				result = "Location='" + location.getAttribute("area") + "'";
			} else if (type.equals("Trajectory")) {
				result = "Location='" + location.getAttribute("trajectory") + "'";
			}
		} else if (stateId.equals("Terminated")){
			if(type.equals("Area")){
				result = "Location='" + location.getAttribute("area") + "'";
			} else {
				result = "Location='" + location.getAttribute("end") + "'";
			}
		}
		return result;
	}

	private String getTime(Element startTime, Element endTime) {
		String result = "";
		String type = startTime.getAttribute("type");
		if (type != null) {
			result = result + "Start=" + type;
			if (!type.equals("ASAP")) {
				result = result + "(" + startTime.getAttribute("value") + "),";
			} else {
				result = result +",";
			}
		}
		type = endTime.getAttribute("type");
		if (type != null) {
			result = result + "End=" + type;
			if (!type.equals("ASAP")) {
				result = result + "(" + endTime.getAttribute("value") + ")";
			} else {
				result = result + ",";
			}
		}
		return result;
	}
}
