package it.polimi.isgroup.qos5gbpmn2vld.dataStructures;

import java.util.ArrayList;

public class VLDConfig {

	public static final String DEFAULTVENDOR = "polimi";
	public static final String DEFAULTVERSION = "0.1";
	public static final String DEFAULTTESTACCESS = "none";
	public static final String DEFAULTCONNECTIVITYTYPE = "E-LAN";
	
	private String id;
	private String vendor;
	private String descriptor_version;
	private String description;
	private String root_requirement;
	private String test_access;
	private ArrayList<QoSMetric> qos;
	private String connectivity_type;
	
	public VLDConfig(){
		this.vendor = DEFAULTVENDOR;
		this.descriptor_version = DEFAULTVERSION;
		this.test_access = DEFAULTTESTACCESS;
		this.connectivity_type = DEFAULTCONNECTIVITYTYPE;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public String getDescriptor_version() {
		return descriptor_version;
	}
	public void setDescriptor_version(String descriptor_version) {
		this.descriptor_version = descriptor_version;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getRoot_requirement() {
		return root_requirement;
	}
	public void setRoot_requirement(String root_requirement) {
		this.root_requirement = root_requirement;
	}
	public String getTest_access() {
		return test_access;
	}
	public void setTest_access(String test_access) {
		this.test_access = test_access;
	}
	public String getConnectivity_type() {
		return connectivity_type;
	}
	public void setConnectivity_type(String connectivity_type) {
		this.connectivity_type = connectivity_type;
	}
	public ArrayList<QoSMetric> getQos() {
		return qos;
	}
	public void setQos(ArrayList<QoSMetric> qos) {
		this.qos = qos;
	} 
}
